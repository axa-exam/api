﻿// <auto-generated />
using AXA.Exam.API.DBContexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AXA.Exam.API.Migrations
{
    [DbContext(typeof(AXAExamSampleDBContext))]
    [Migration("20200709080058_axaexamsampledb")]
    partial class axaexamsampledb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AXA.Exam.API.Models.Applicants", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .HasColumnType("varchar(255)")
                        .HasMaxLength(255)
                        .IsUnicode(false);

                    b.Property<string>("Mobile")
                        .HasColumnType("varchar(255)")
                        .HasMaxLength(255)
                        .IsUnicode(false);

                    b.Property<string>("Name")
                        .HasColumnType("varchar(255)")
                        .HasMaxLength(255)
                        .IsUnicode(false);

                    b.Property<string>("PositionApplied")
                        .HasColumnType("varchar(255)")
                        .HasMaxLength(255)
                        .IsUnicode(false);

                    b.Property<string>("Source")
                        .HasColumnType("varchar(255)")
                        .HasMaxLength(255)
                        .IsUnicode(false);

                    b.Property<string>("XaxaApiKey")
                        .HasColumnName("XAxaApiKey")
                        .HasColumnType("varchar(255)")
                        .HasMaxLength(255)
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.HasIndex("XaxaApiKey")
                        .IsUnique()
                        .HasName("IX_Applicants")
                        .HasFilter("[XAxaApiKey] IS NOT NULL");

                    b.ToTable("Applicants");
                });

            modelBuilder.Entity("AXA.Exam.API.Models.Resumes", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ApplicantId")
                        .HasColumnName("ApplicantID")
                        .HasColumnType("int");

                    b.Property<string>("Resume")
                        .IsRequired()
                        .HasColumnName("resume")
                        .HasColumnType("varchar(1000)")
                        .HasMaxLength(1000)
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.HasIndex("ApplicantId");

                    b.ToTable("Resumes");
                });

            modelBuilder.Entity("AXA.Exam.API.Models.Schedules", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ApplicantId")
                        .HasColumnName("ApplicantID")
                        .HasColumnType("int");

                    b.Property<string>("ProposedDate")
                        .IsRequired()
                        .HasColumnType("varchar(20)")
                        .HasMaxLength(20)
                        .IsUnicode(false);

                    b.Property<string>("ProposedTime")
                        .IsRequired()
                        .HasColumnType("varchar(5)")
                        .HasMaxLength(5)
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.HasIndex("ApplicantId");

                    b.ToTable("Schedules");
                });

            modelBuilder.Entity("AXA.Exam.API.Models.Resumes", b =>
                {
                    b.HasOne("AXA.Exam.API.Models.Applicants", "Applicant")
                        .WithMany("Resumes")
                        .HasForeignKey("ApplicantId")
                        .HasConstraintName("FK_Resumes_Applicants")
                        .IsRequired();
                });

            modelBuilder.Entity("AXA.Exam.API.Models.Schedules", b =>
                {
                    b.HasOne("AXA.Exam.API.Models.Applicants", "Applicant")
                        .WithMany("Schedules")
                        .HasForeignKey("ApplicantId")
                        .HasConstraintName("FK_ApplicantID")
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
