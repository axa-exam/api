﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AXA.Exam.API.Migrations
{
    public partial class axaexamsampledb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Applicants",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Email = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Mobile = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    PositionApplied = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Source = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    XAxaApiKey = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applicants", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Resumes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicantID = table.Column<int>(nullable: false),
                    resume = table.Column<string>(unicode: false, maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resumes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Resumes_Applicants",
                        column: x => x.ApplicantID,
                        principalTable: "Applicants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Schedules",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicantID = table.Column<int>(nullable: false),
                    ProposedDate = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ProposedTime = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedules", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ApplicantID",
                        column: x => x.ApplicantID,
                        principalTable: "Applicants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Applicants",
                table: "Applicants",
                column: "XAxaApiKey",
                unique: true,
                filter: "[XAxaApiKey] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Resumes_ApplicantID",
                table: "Resumes",
                column: "ApplicantID");

            migrationBuilder.CreateIndex(
                name: "IX_Schedules_ApplicantID",
                table: "Schedules",
                column: "ApplicantID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Resumes");

            migrationBuilder.DropTable(
                name: "Schedules");

            migrationBuilder.DropTable(
                name: "Applicants");
        }
    }
}
