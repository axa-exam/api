﻿using AXA.Exam.API.Classes;
using AXA.Exam.API.DBContexts;
using AXA.Exam.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace AXA.Exam.API.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class UploadController : ControllerBase {
        private readonly ILogger<UploadController> _logger;
        private readonly IHostEnvironment _host;

        public UploadController(ILogger<UploadController> logger, IHostEnvironment host) {
            _logger = logger;
            _host = host;
        }

        [HttpPost]
        public object Post([FromBody] UploadDal body) {
            if (HttpContext.Request.ContentType != "application/json") {
                return StatusCode(406, new ErrorResult() { Result = 406, ErrMessage = "ContentType should be 'application/json'" });
            }
            HttpContext.Request.Headers.TryGetValue("x-axa-api-key", out var apiKey);
            if (apiKey.Count == 0) {
                return StatusCode(406, new ErrorResult() { Result = 406, ErrMessage = "x-axa-api-key should be supplied" });
            }

            using (var context = new AXAExamSampleDBContext()) {
                var existApplicant = context.Applicants.Where(o => o.XaxaApiKey == apiKey.First()).FirstOrDefault();
                if (existApplicant == null) {
                    return StatusCode(406, new ErrorResult() { Result = 406, ErrMessage = "Invalid x-axa-api-key. Make sure you've input valid x-axa-api-key." });
                }

                try {

                    var wwwRoot = _host.ContentRootPath;
                    var file = $"\\Resumes\\{apiKey.First()}-{DateTime.Now.Ticks}.pdf";
                    var filepath = wwwRoot + file;

                    byte[] bytes = Convert.FromBase64String(body.File.Data.Split(',')[1]);

                    var stream = new FileStream(filepath, FileMode.CreateNew);
                    var writer = new BinaryWriter(stream);
                    writer.Write(bytes, 0, bytes.Length);
                    writer.Close();


                    var resumes = new Resumes() {
                        Resume = file,
                        ApplicantId = existApplicant.Id,
                    };

                    context.Resumes.Add(resumes);
                    context.SaveChanges();

                } catch (Exception ex) {
                    return StatusCode(500, new ErrorResult() { Result = 500, ErrMessage = ex.Message });
                }

            }


            return StatusCode(200, new SuccessResult() {
                Result = 200,
                Message = $"Upload successful"
            });
        }
    }
}
