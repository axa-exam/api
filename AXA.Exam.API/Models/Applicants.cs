﻿using System;
using System.Collections.Generic;

namespace AXA.Exam.API.Models
{
    public partial class Applicants
    {
        public Applicants()
        {
            Resumes = new HashSet<Resumes>();
            Schedules = new HashSet<Schedules>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string PositionApplied { get; set; }
        public string Source { get; set; }
        public string XaxaApiKey { get; set; }

        public virtual ICollection<Resumes> Resumes { get; set; }
        public virtual ICollection<Schedules> Schedules { get; set; }
    }
}
