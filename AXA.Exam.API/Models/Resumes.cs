﻿using System;
using System.Collections.Generic;

namespace AXA.Exam.API.Models {
    public partial class Resumes
    {
        public int Id { get; set; }
        public int ApplicantId { get; set; }
        public string Resume { get; set; }

        public virtual Applicants Applicant { get; set; }
    }
}
